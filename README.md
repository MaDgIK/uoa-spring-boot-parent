# uoa-spring-boot-parent

This parent contains the global configuration for:

- Parent [uoa-global-parent](https://code-repo.d4science.org/MaDgIK/uoa-global-parent)
- Dependency Management for Spring boot.
- Spring boot basic dependencies
- [Google Gson Dependency](https://mvnrepository.com/artifact/com.google.code.gson/gson)
- Build Configuration:
  - Plugin Management for spring maven plugin.
  - Resources directory
- Global properties for spring-boot, google-gson and spring-doc (Swagger)

## Usage

    <parent>
        <groupId>eu.dnetlib</groupId>
        <artifactId>uoa-spring-boot-parent</artifactId>
        <version>2.0.3</version>
    </parent>
